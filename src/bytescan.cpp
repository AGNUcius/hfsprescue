/*
 *
 * Released under GPL v2
 * by Elmar Hanlhofer  http://www.plop.at
 *
 */


#define _FILE_OFFSE_BITS 64
#define _LARGEFILE64_SOURCE

#include "config.h"

#include "apple.h"
#include "freebsd.h"

#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <inttypes.h>
    


#include <stdlib.h>
#include <string.h>

#include "bytescan.h"
#include "tools.h"
#include "log.h"
#include "utf8mac.h"



char unicode[1024];
size_t unicode_len;


bool ScanForUnicode (char *buffer, int size, char *str, uint64_t fpos)
{
    bool ret = false;
    for (int i = 0; i < size - unicode_len; i++)
    {
	if (memcmp (buffer + i, unicode, unicode_len) == 0)
	{
    	    LogPrn ("String \"%s\" found at offset %" PRIu64 " + %d = %" PRIu64 " (0x%llx + 0x%x = 0x%llx)", str, fpos, i, fpos + i, fpos, i, fpos + i);
    	    ret = true;
	}
    }
    
    return ret;
}

bool ScanForBytes (char *buffer, int size, char *buffer2, int size2, uint64_t fpos)
{
    bool ret = false;
    for (int i = 0; i < size - size2; i++)
    {
	if (memcmp (buffer + i, buffer2, size2) == 0)
	{
    	    LogPrn ("File bytes found at offset %" PRIu64 " + %d = %" PRIu64 " (0x%llx + 0x%x = 0x%llx)", fpos, i, fpos + i, fpos, i, fpos + i);
    	    ret = true;
	}    
    }
    
    return ret;
}

#define OUTPUT_LIMIT 4
void ByteScan (char *file, char *find_string, char *find_file, int num_bytes, uint64_t offset)
{
    if (num_bytes > 1024 * 1024)
    {
	fprintf (stderr, "Error: max size is %d (1MB)\n", 1024 * 1024);
	exit (EXIT_FAILURE);
    }

    FILE *pf;
    char buffer[1024 * 1024];
    char buffer_file[num_bytes];
    int mb = 0;
    int out = OUTPUT_LIMIT;
    uint64_t fpos = offset;
    int fd;
    
    size_t consumed;

    if (!find_file[0] && !find_string[0])
    {
	fprintf (stderr, "Error: For --find you also have to use -fs and/or -fi\n");
	exit (EXIT_FAILURE);
    }

    
    if (find_file[0])
    {
	pf = fopen (find_file, "r");
	if (!pf)
	{
	    fprintf (stderr, "Unable to open '%s': %s\n", find_file, strerror (errno));
	    exit (EXIT_FAILURE);
	}    
	fread (buffer_file, num_bytes, 1, pf);    
	fclose (pf);
    }
    
    if (offset > 0)
    {
        LogPrn ("*** Skipping %" PRIu64 " bytes (%" PRIu64 " MB)", offset, offset / 1024 / 1024);
    }
    
    
    if (find_string[0])
    {
	int ret;
	ret = utf8_decodestr ((u_int8_t*)find_string, strlen (find_string),
            		      (u_int16_t *)unicode, &unicode_len, sizeof (unicode),
            		      0, 0,
            		      &consumed);
            		      
        if (ret == ENAMETOOLONG)
        {
    	    LogPrn ("Name didn't fit; only %d chars were decoded.", unicode_len);
    	}
    	
    	if (ret == EINVAL)
    	{
	    LogPrn ("Illegal UTF-8 sequence found.");
    	    exit (EXIT_FAILURE);
    	}

	// swap unicode bytes  (not implemented in utf8_decodestr)
	for (int i = 0; i < unicode_len; i += 2)
	{
	    char c = unicode[i];
	    unicode[i] = unicode[i + 1];
	    unicode[i + 1] = c;
	}
    }


    fd = open64 (file, O_RDONLY);
    if (fd < 0)
    {
	fprintf (stderr, "Unable to open %s: %s\n", file, strerror (errno));
	exit (EXIT_FAILURE);
    }    

    while (pread64 (fd, buffer, sizeof (buffer), fpos))
    {
	if (find_string[0]) 
	{
	    if (ScanForUnicode (buffer, sizeof (buffer), find_string, fpos))
	    {
		out = OUTPUT_LIMIT;
	    }
	}
	if (find_file[0]) 
	{
	    if (ScanForBytes   (buffer, sizeof (buffer), buffer_file, num_bytes, fpos))
	    {
		out = OUTPUT_LIMIT;
	    }
	}
	    
	mb++;	
	out++;
	if (out >= OUTPUT_LIMIT)
	{
	    printf ("%d MB scanned \r", mb);
	    fflush (stdout);
	    out = 0;
	}
	fpos += 1024 * 1024;
    }
    printf ("\n");
    close (fd);
}
