#ifndef _BYTESCAN_H_
#define _BYTESCAN_H_

void ByteScan (char *file, char *find_string, char *find_file, int num_bytes, uint64_t offset);

#endif